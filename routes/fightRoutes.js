const { Router } = require('express');
const FightService = require('../services/fightService');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();


router.get('/', function(req, res, next) {
    const data = FightService.getAll();
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not get fights');
    }
    next();
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
    const data = FightService.search({ id: req.params.id });
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not get fight by Id');
    }
    next();
}, responseMiddleware);

router.post('/', function(req, res, next) {
    const data = FightService.create(req.body);
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not create fight');
    }
    next();
}, responseMiddleware);

router.put('/:id', function(req, res, next) {
    const data = FightService.update(req.params.id, req.body);
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not update fight');
    }
    next();
}, responseMiddleware);


router.delete('/:id', function(req, res, next) {
    const data = FightService.delete(req.params.id);
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not delete fight');
    }
    next();
}, responseMiddleware);

module.exports = router;