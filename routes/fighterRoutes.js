const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();


router.get('/', function(req, res, next) {
    const data = FighterService.getAll();
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not get fighters');
    }
    next();
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
    const data = FighterService.search({ id: req.params.id });
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not get fighter by Id');
    }
    next();
}, responseMiddleware);

router.post('/', createFighterValid, function(req, res, next) {
    const data = FighterService.create(req.body);
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not create fighter');
    }
    next();
}, responseMiddleware);

router.put('/:id', updateFighterValid, function(req, res, next) {
    const data = FighterService.update(req.params.id, req.body);
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not update fighter');
    }
    next();
}, responseMiddleware);


router.delete('/:id', function(req, res, next) {
    const data = FighterService.delete(req.params.id);
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not delete fighter');
    }
    next();
}, responseMiddleware);

module.exports = router;