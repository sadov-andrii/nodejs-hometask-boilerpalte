const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function(req, res, next) {
    const data = UserService.getAll();
    if (data) {
        res.data = data;
    } else {
        res.err = new Error('Could not get users');
    }
    next();
}, responseMiddleware);

router.get('/:id', function(req, res, next) {
    const data = UserService.search({ id: req.params.id });
    if(data) {
        res.data = data;
    } else {
        res.err = new Error('Could not get user by Id');
    }
    next();
}, responseMiddleware);

router.post('/', createUserValid, function(req, res, next) {
    const data = UserService.create(req.body);
    if(data) {
        res.data = data;
    } else {
        res.err = new Error('Could create user');
    }
    next();
}, responseMiddleware);

router.put('/:id', updateUserValid, function(req, res, next) {
    const data = UserService.update(req.params.id, req.body);
    if(data) {
        res.data = data;
    } else {
        res.err = new Error('Could not update user');
    }
    next();
}, responseMiddleware);


router.delete('/:id', function(req, res, next) {
    const data = UserService.delete(req.params.id);
    if(data) {
        res.data = data;
    } else {
        res.err = new Error('Could not delete user');
    }
    next();
}, responseMiddleware);


module.exports = router;