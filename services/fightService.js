const { FightRepository } = require('../repositories/fightRepository');

class FightService {
    search(search) {
        const item = FightRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll() {
        const items = FightRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }
    
    create(data) {
        const item = FightRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, data) {
        const item = FightRepository.update(id, data);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = FightRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FightService();