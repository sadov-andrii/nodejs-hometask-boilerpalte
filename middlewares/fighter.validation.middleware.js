const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    const data = req.body;
    const errMessage = validateId(data) || 
                       validatePower(data) || 
                       validateDefense(data) ||
                       validateName(data) ||
                       validateCreateNameExists(data) ||
                       validateNeedlessFields(data) || 
                       validateMissingFields(data);
    if (errMessage) {
        const error = {
            error: true,
            message: errMessage + " on create"
        };
        res.status(400).send(error);
    } else {
        next();
    }
};

const updateFighterValid = (req, res, next) => {
    const data = req.body;
    const errMessage = validateId(data) || 
                       validatePower(data) || 
                       validateDefense(data) ||
                       validateName(data) ||
                       validateUpdateNameExists(data, req.params.id) ||
                       validateNeedlessFields(data);
    if (errMessage) {
        const error = {
            error: true,
            message: errMessage + " on update"
        };
        res.status(400).send(error);
    } else {
        next();
    }
};

function validateId(data) {
    if ('id' in data) {
        return 'Id in fighter entity';
    }
}

function validatePower(data) {
    if (('power' in data) && !(Number(data.power) && +data.power < 100 && +data.power >= 1)) {
        return 'Not valid power in fighter entity  (more than 100)';
    }
}

function validateDefense(data) {
    if (('defense' in data) && !(Number(data.defense) && +data.defense <= 10 && +data.defense >= 1)) {
        return 'Not valid defense in fighter entity (more than 10 or less than 1)';
    }
}

function validateName(data) {
    if (('name' in data) && (data.name.length <= 2)) {
        return 'Not valid name in fighter entity (less than 2 character)';
    }
}

function validateCreateNameExists(data) {
    if (('name' in data) && FighterService.search({name: data.name})){
        return 'Fighter name exists';
    }
}

function validateUpdateNameExists(data, id) {
    if ('name' in data) {
        const result = FighterService.search({name: data.name});
        if (result && (result.id != id)) {
            return 'Fighter name exists';
        }
    }
}

function validateNeedlessFields(data) {
    for (let key in data) {
        if (! (key in fighter)) {
            return 'Needless fields in fighter entity';
        }
    }
}

function validateMissingFields(data) {
    if (Object.keys(data).length != Object.keys(fighter).length - 1) {
        return 'Missing fields in fighter entity';
    }
}
    

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;