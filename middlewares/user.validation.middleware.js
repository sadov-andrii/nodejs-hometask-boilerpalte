const { user } = require('../models/user');

const UserService = require('../services/userService');

const createUserValid = (req, res, next) => {
    const data = req.body;
    const errMessage = validateId(data) || 
                       validateEmail(data) ||
                       validateCreateEmailExists(data) ||
                       validatePhone(data) || 
                       validatePassword(data) ||
                       validateName(data) ||
                       validateNeedlessFields(data) || 
                       validateMissingFields(data);
    if (errMessage) {
        const error = {
            error: true,
            message: errMessage + " on create"
        };
        res.status(400).send(error);
    } else {
        next();
    }
};

const updateUserValid = (req, res, next) => {
    const data = req.body;
    const errMessage = validateId(data) || 
                       validateEmail(data) ||
                       validateUpdateEmailExists(data, req.params.id) ||
                       validatePhone(data) || 
                       validatePassword(data) ||
                       validateName(data) || 
                       validateNeedlessFields(data);
    if (errMessage) {
        const error = {
            error: true,
            message: errMessage + " on update"
        };
        res.status(400).send(error);
    } else {
        next();
    }
};

function validateId(data) {
    if ('id' in data) {
        return 'Id in user entity';
    }
}

function validateEmail(data) {
    if (('email' in data) && !data.email.endsWith('@gmail.com')) {
        return 'Not valid email (gmail) in user entity';
    }
}

function validatePassword(data) {
    if (('password' in data) && data.password.length <= 3) {
        return 'Not valid password (less than 3 character) in user entity';
    }
}

function validateName(data) {
    if (('name' in data) && data.name.length <= 2) {
        return 'Not valid name (less than 2 character) in user entity';
    }
}

function validateCreateEmailExists(data) {
    if (('email' in data) && UserService.search({email: data.email})){
        return 'User email exists';
    }
}

function validateUpdateEmailExists(data, id) {
    if ('email' in data) {
        const result = UserService.search({email: data.email});
        if (result && (result.id != id)) {
            return 'User email exists';
        }
    }
}

function validatePhone(data) {
    if (('phoneNumber' in data) && (
         !data.phoneNumber.startsWith('+380') || 
         !(data.phoneNumber.length == 13))) {
        return 'Not valid phone number (+380xxxxxxxxx) in user entity';
    }
}

function validateNeedlessFields(data) {
    for (let key in data) {
        if (! (key in user)) {
            return 'Needless fields in fighter entity';
        }
    }
}

function validateMissingFields(data) {
    if (Object.keys(data).length != Object.keys(user).length - 1) {
        return 'Missing fields in user entity';
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;