const responseMiddleware = (req, res, next) => {
   if (res.err) {
     const error = {
        error: true,
        message: res.err.message
     };
     res.status(404).send(error);       
   } else {
     res.status(200).send(res.data);
   }
   next();
};

exports.responseMiddleware = responseMiddleware;